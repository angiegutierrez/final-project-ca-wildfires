# Search California Wildfire Data using Pandas DataFrame

### This reusable notebook for use in Jupyter allows a user to search through California fire data using dropdown menu features. Users can select by year and county, and the output will be a Pandas DataFrame along with the total number of acres that have been burned from fires.

| Snapshot of Pandas DataFrame                | 
| :------------------------------:            |
| ![DataFrame](images/dataframe_snapshot.PNG) |


### Instructions

The user must have Jupyter notebook or a Python working environment that has Python 3 installed.

There are no data files to download since the notebook uses data pulled directly from CalFire's website via URL.
The .csv data used for this notebook contains
     - Fire incident name, year, county, acres of land burned, date the fire started, date extinguished, latitude and longitude. 
     
To use the notebook, simply run the code and the dropdown menu will display after the last code cell. From there, select from the dropdown menus what year and county you would like to see data. 


### Things to Consider

Because data is acquired directly from a live website, we are able to use the most up-to-date data. However, some of the data  is a bit inaccurate. The .csv contains data for 1969 in Marin County and Merced County, but lists the Date Extinguised as 2019 and 2018, respectively. It is clear that there is some discrepancy, so it is recommended that the user exercise caution when utilizing this data. 


### Repository Contents

- Jupyter notebook that contains the dropdown menu to select data
- .py file containing raw python code for use in another Python working environment
- Sample .csv data file (downloaded as of: 12-13-2020)

### Sources used
- CalFire data (link will download file): https://www.fire.ca.gov/imapdata/mapdataall.csv
- [Bring your Jupyter notebook to life with interactive widgets](https://towardsdatascience.com/bring-your-jupyter-notebook-to-life-with-interactive-widgets-bc12e03f0916)
- [Ipywidgets: Widget Lists](https://ipywidgets.readthedocs.io/en/latest/examples/Widget%20List.html)


