#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Begin my importing necessary modules
import pandas as pd
import csv
import ipywidgets as widgets


# In[2]:


#By using the .csv data file found directly on CalFire's website, the user gets the most up-to-date data.
# set up dataframe with a variable
# set URL variable to Cal Fire's website

filtered = pd.DataFrame()
url = 'https://www.fire.ca.gov/imapdata/mapdataall.csv'

#read csv file
df=pd.read_csv(url)


# In[3]:


#The original .csv file has much more information than needed. To filter out only the year information from 
#the 'incident_date_created' column to extract only the year, use Pandas DateTimeIndex object.
#We need to do this because the incident_date_created field comes out with month, day, year, hours, mins, secs, etc
#We only want to filter on year 
df['year'] = pd.DatetimeIndex(df['incident_date_created']).year

# Here I choose which columns I would like to display in the outputs
df = df[['year','incident_name','incident_county','incident_acres_burned',
         'incident_dateonly_created','incident_dateonly_extinguished',
         'incident_longitude','incident_latitude']]

#rename the column headings for neatness
df.columns = ['Year','Incident', 'County','Acres Burned','Date Started',
              'Date Extinguished','Longitude','Latitude']


# In[4]:


#The purpose of this code is for users to be able to see how many acres of land were 
#burned in a county during a specific year. To do this, use dropdown menus (one for 
#county and one for year) for the user to make selections easily. 
#The Widgets module allows for the creation of dropdown menus, so we will use that.

#These 2 lines pertains to the 'year' and  'county' dropdown menus. 
#Because we don't want the years or counties to be duplicated in the dropdown, 
#we will sort based on 'unique' values.  
dropdown_year = widgets.Dropdown(options = sorted(df.Year.unique()))
dropdown_county = widgets.Dropdown(options = sorted(df.County.astype(str).unique()))


# In[5]:


#When there is a change triggered in either dropdown menu, common_filtering will 
#be called and the eventhandler function will apply a filter. Example: when selecting 
#a different year in the dropdown menu, filtering of data is applied.

def dropdown_year_eventhandler(change):
        common_filtering(change.new, dropdown_county.value)
def dropdown_county_eventhandler(change):
        common_filtering(dropdown_year.value, change.new)

#'observe' will watch for any changes. If a change occurs, it will activate the above code lines.
dropdown_year.observe(dropdown_year_eventhandler, names='value')
dropdown_county.observe(dropdown_county_eventhandler, names='value')

#this will total up acres burned in the year and county that are selected through filtering at the bottom of the table
def common_filtering(year, county):
        output.clear_output()

        total_filter = df[(df.Year == year) & (df.County == county)]
        summed_acres = total_filter['Acres Burned'].sum()
        
        with output:
            display(total_filter)
            print('Total Acres Burned in '+str(year)+' in ' + str(county)+' County: ' + str(summed_acres))


# In[6]:


#Final step is to create the output dropdown menus
#Hbox arranges the dropdown menu widgets from left to right horizontally.
input_widgets = widgets.HBox([dropdown_year, dropdown_county])
display(input_widgets)

#display the output
output = widgets.Output()
display(output)

